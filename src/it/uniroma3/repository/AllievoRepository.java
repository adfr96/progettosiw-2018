package it.uniroma3.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import it.uniroma3.spring.model.Allievo;

public interface AllievoRepository extends CrudRepository<Allievo, Long>{
	
	List<Allievo> findByNome(String nome);
	
	List<Allievo> findByCognome(String nome);
	
	List<Allievo> findByEmail(String email);
}
