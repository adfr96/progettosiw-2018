package it.uniroma3.repository;

import org.springframework.data.repository.CrudRepository;
import java.util.List;
import it.uniroma3.spring.model.Attivita;

public interface AttivitaRepository extends CrudRepository<Attivita, Long>{
	
	List<Attivita> findByNome(String nome);
}
