package it.uniroma3.repository;

import org.springframework.data.repository.CrudRepository;
import java.util.List;
import it.uniroma3.spring.model.Centro;

public interface CentroRepository extends CrudRepository<Centro, Long>{

	List<Centro> findByNome(String nome);
	
	List<Centro> findByEmail(String email);
}
