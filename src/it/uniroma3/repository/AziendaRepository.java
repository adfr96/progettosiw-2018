package it.uniroma3.repository;

import org.springframework.data.repository.CrudRepository;
import it.uniroma3.spring.model.Azienda;

public interface AziendaRepository extends CrudRepository<Azienda, Long>{

}
