package it.uniroma3.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class Attivita {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	private String nome;
	
	private Date data_ora;
	
	@ManyToMany
	private List<Allievo> allievi;
	
	@ManyToOne
	private Centro centro;
	
	public Attivita(String nome, Date data_ora,Centro centro) {
		super();
		this.nome = nome;
		this.data_ora = data_ora;
		this.centro = centro;
		this.allievi = new ArrayList<>();
	}

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}


	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getData_ora() {
		return data_ora;
	}

	public void setData_ora(Date data_ora) {
		this.data_ora = data_ora;
	}
	
	
}
