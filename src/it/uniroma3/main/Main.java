package it.uniroma3.main;

import javax.persistence.EntityManager;

import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import it.uniroma3.model.Centro;

public class Main {

	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("azienda-unit");
		EntityManager em = emf.createEntityManager();
		/*
		 * prova per vedere se funzionano le librerie 
		 * */
		Centro centro = new Centro("centro1","Via aaa","aaa@bbb.cc","1111111111",30);
		
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		em.persist(centro);
		tx.commit();

		em.close();
		emf.close();

	}

}
